#include "snifferwidget.h"
#include <QPushButton>
#include <QBoxLayout>
#include <QLabel>
#include <QLineEdit>

SnifferWidget::SnifferWidget(QWidget *parent)
    : QWidget(parent)
{
    sniffer = new Sniffer(&cb);

    gen = new PacketGenerator(sniffer);
    connect(gen, SIGNAL(flowUpdated(float)), SLOT(onFlowUpdated(float)));
    connect(gen, SIGNAL(packetsCountUpdated(quint64)), SLOT(onPacketsCountUpdated(quint64)));

    connect(&cb, SIGNAL(binCounterUpdated(quint64)), SLOT(onBinCounterUpdated(quint64)));
    connect(&cb, SIGNAL(txtCounterUpdated(quint64)), SLOT(onTxtCounterUpdated(quint64)));

    startGenBtn = new QPushButton;
    startGenBtn->setText("Start generator");
    connect(startGenBtn, SIGNAL(clicked()), SLOT(onGenStartClicked()));

    stopGenBtn = new QPushButton;
    stopGenBtn->setEnabled(false);
    stopGenBtn->setText("Stop generator");
    connect(stopGenBtn, SIGNAL(clicked()), SLOT(onGenStopClicked()));

    binaryPacketsEdit = new QLineEdit;
    binaryPacketsEdit->setFixedWidth(200);
    binaryPacketsEdit->setReadOnly(true);
    auto hbl1 = new QHBoxLayout;
    hbl1->addWidget(new QLabel("Binary qty:"));
    hbl1->addWidget(binaryPacketsEdit);

    textPacketsEdit = new QLineEdit;
    textPacketsEdit->setFixedWidth(200);
    textPacketsEdit->setReadOnly(true);
    auto hbl2 = new QHBoxLayout;
    hbl2->addWidget(new QLabel("Text qty:"));
    hbl2->addWidget(textPacketsEdit);

    totalPacketsEdit = new QLineEdit;
    totalPacketsEdit->setFixedWidth(200);
    totalPacketsEdit->setReadOnly(true);
    auto hbl3 = new QHBoxLayout;
    hbl3->addWidget(new QLabel("Total qty:"));
    hbl3->addWidget(totalPacketsEdit);

    flowEdit = new QLineEdit;
    flowEdit->setFixedWidth(200);
    flowEdit->setReadOnly(true);
    auto hbl4 = new QHBoxLayout;
    hbl4->addWidget(new QLabel("Flow (mbit/s):"));
    hbl4->addWidget(flowEdit);

    QVBoxLayout *vbl = new QVBoxLayout;
    vbl->addWidget(startGenBtn);
    vbl->addWidget(stopGenBtn);
    vbl->addLayout(hbl1);
    vbl->addLayout(hbl2);
    vbl->addLayout(hbl3);
    vbl->addLayout(hbl4);

    setLayout(vbl);

    resize(300, 100);
}

SnifferWidget::~SnifferWidget()
{
    gen->disable();
    pool.waitForDone();
    delete sniffer;
}

void SnifferWidget::onGenStartClicked()
{
    startGenBtn->setEnabled(false);
    stopGenBtn->setEnabled(true);
    pool.start(gen);
}

void SnifferWidget::onGenStopClicked()
{
    stopGenBtn->setEnabled(false);
    startGenBtn->setEnabled(true);
    gen->disable();
}

void SnifferWidget::onFlowUpdated(float value)
{
    flowEdit->setText(QString::number(value*8));
}

void SnifferWidget::onPacketsCountUpdated(quint64 value)
{
    totalPacketsEdit->setText(QString::number(value));
}

void SnifferWidget::onTxtCounterUpdated(quint64 value)
{
    textPacketsEdit->setText(QString::number(value));
}

void SnifferWidget::onBinCounterUpdated(quint64 value)
{
    binaryPacketsEdit->setText(QString::number(value));
}
