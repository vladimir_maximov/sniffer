#ifndef SNIFFERCALLBACK_H
#define SNIFFERCALLBACK_H

#include <QDebug>
#include <QObject>
#include "icallback.h"

class SnifferCallback : public QObject, public ICallback {
    Q_OBJECT

private:
    quint64 binCounter;
    quint64 txtCounter;

public:
    SnifferCallback(QObject *p = 0)
        : QObject(p), binCounter(0), txtCounter(0) {}

    void BinaryPacket(const char *data, unsigned int size)
    {
        QByteArray ba;
        ba.append(data, size);
        ba.clear();
        binCounter++;
        if (!(binCounter % 500))
            emit binCounterUpdated(binCounter);
    }

    void TextPacket(const char *data, unsigned int size)
    {
        QByteArray ba;
        ba.append(data, size);
        ba.clear();
        txtCounter++;
        if (!(txtCounter % 500))
            emit txtCounterUpdated(txtCounter);
    }

signals:
    void txtCounterUpdated(quint64);
    void binCounterUpdated(quint64);
};

#endif // SNIFFERCALLBACK_H
