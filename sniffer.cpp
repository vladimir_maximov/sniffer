#include "sniffer.h"
#include <QDebug>

Sniffer::Sniffer(ICallback *c)
    : callback(c), readPos(0), writePos(0), action(aHeader), dataSize(0)
{
    binarySize.resize(4);

    result.reserve(defaultResultSize);
    buffer.resize(bufferSize);
}

Sniffer::~Sniffer() { }

void Sniffer::Receive(const char *data, unsigned int size)
{
    // запись входного массива в кольцевой буфер
    //
    unsigned int writeLen = bufferSize - (writePos&mask);
    writeLen = qMin(writeLen, size);

    buffer.replace(writePos&mask, writeLen, data, writeLen);
    writePos += writeLen;

    // достигнут конец кольцевого буфера
    // запись в начало
    //
    unsigned int diff = size - writeLen;
    if (diff) {
        buffer.replace(writePos&mask, diff, data+writeLen, diff);
        writePos += diff;
    }

    process();
}

void Sniffer::process()
{
    while (readPos != writePos) {
        char c = buffer[readPos&mask];

        switch (action) {
        case aHeader:
            // определение типа пакета (текстовые или бинарный)
            //
            clearResult();
            if (c == binaryHeader) {
                binarySizeIndex = 0;
                ++readPos;
                action = aBinarySize;
            } else
                action = aCR1;
            break;
        case aCR1:
            // первый '/r'
            //
            addValue(c);
            if (c == cr)
                action = aLF1;
            ++readPos;
            break;
        case aLF1:
            // первый '/n'
            //
            if (c == lf) {
                addValue(c);
                ++readPos;
                action = aCR2;
            } else
                action = aCR1;
            break;
        case aCR2:
            // первый '/r'
            //
            if (c == cr) {
                addValue(c);
                ++readPos;
                action = aLF2;
            } else
                action = aCR1;
            break;
        case aLF2:
            // второй '/n'
            //
            if (c == lf) {
                addValue(c);
                ++readPos;
                // отправить данные текстового пакета
                callbackTextPacket(result.constData(), result.size() - 4);
                action = aHeader;
            } else
                action = aCR1;
            break;
        case aBinarySize:
            // определение размера данных бинарного пакета
            //
            binarySize[binarySizeIndex++] = c;

            if (binarySizeIndex > 3) {
                dataSize = *((int*)binarySize.data());

                if (dataSize) {
                    // увеличить буфер результатов до размера бинарного
                    // пакета, если требуется
                    //
                    extendResult(dataSize);
                    action = aBinaryData;
                } else {
                    // бинарный пакет без данных
                    callbackBinaryPacket(0, 0);
                    action = aHeader;
                }
            }
            ++readPos;
            break;
        case aBinaryData:
            // чтение данных бинарного пакета
            //
            addValue(c);
            --dataSize;

            if (!dataSize) {
                // отправить данные бинарного пакета
                callbackBinaryPacket(result.constData(), result.size());
                // перейти к следующему пакету
                action = aHeader;
            }
            ++readPos;
            break;
        }
    }
}
