#ifndef IRECEIVER_H
#define IRECEIVER_H

struct IReceiver
{
    virtual void Receive(const char *data, unsigned int size) = 0;
};

#endif // IRECEIVER_H
