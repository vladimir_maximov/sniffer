#include "packetgenerator.h"
#include <QTime>
#include <QDebug>

PacketGenerator::PacketGenerator(IReceiver *rcv, QObject *p)
    : QObject(p), rcv(rcv), size(0), offset(0), sendSize(0), fullSize(0)
{
    setAutoDelete(false);

    data.reserve(0x10000);
    data.fill('A');

    QTime midnight(0, 0, 0);
    qsrand(midnight.secsTo(QTime::currentTime()));
}

int PacketGenerator::generatePacket()
{
    return (qrand() % 2) ? generateTextPacket() : generateBinaryPacket();
}

int PacketGenerator::generateTextPacket()
{
    int len = randPacketSize();
    data.fill('A', len);
    data.replace(len, 4, "\r\n\r\n", 4);
    return len + 4;
}

int PacketGenerator::generateBinaryPacket()
{
    int len = randPacketSize();
    data.fill(0x24, len + 5);
    data.replace(1, 4, (char*)&len, 4);
    return len + 5;
}

int PacketGenerator::randPacketSize()
{
    return qrand() % (0x10000 - 5);
}

int PacketGenerator::randSize()
{
    return qrand() % (2150 - 5);
}

void PacketGenerator::run() {
    enabled = true;
    QElapsedTimer timer;
    QElapsedTimer t2;
    int msecsElapsed = 0;
    t2.start();
    quint64 packetsCounter = 0;
    while(enabled) {
        timer.start();
        if (!size) {
            offset = 0;
            size = generatePacket();
            packetsCounter++;
            if (!(packetsCounter % 1000))
                emit packetsCountUpdated(packetsCounter);
        }
        sendSize = randSize();
        sendSize = sendSize > size ? size : sendSize;
        rcv->Receive(data.constData() + offset, sendSize);
        offset += sendSize;
        size -= sendSize;
//        while (timer.nsecsElapsed() < 13000);

        fullSize += sendSize;
        msecsElapsed = t2.elapsed();
        if (msecsElapsed > 1000) {
            emit flowUpdated(((float)(fullSize / (1024*1024))/msecsElapsed)*1000.0);
            fullSize = 0;
            t2.start();
        }
    }
}

void PacketGenerator::disable()
{
    enabled = false;
}
