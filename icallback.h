#ifndef ICALLBACK_H
#define ICALLBACK_H

struct ICallback
{
    virtual void BinaryPacket(const char *data, unsigned int size) = 0;
    virtual void TextPacket(const char *data, unsigned int size) = 0;
};

#endif // ICALLBACK_H
