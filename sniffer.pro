#-------------------------------------------------
#
# Project created by QtCreator 2016-03-07T21:42:44
#
#-------------------------------------------------

QT += core gui widgets concurrent

TARGET = sniffer
TEMPLATE = app


SOURCES += main.cpp\
        snifferwidget.cpp \
    sniffer.cpp \
    packetgenerator.cpp \
    sniffercallback.cpp

HEADERS  += snifferwidget.h \
    icallback.h \
    ireceiver.h \
    sniffer.h \
    packetgenerator.h \
    sniffercallback.h
