#ifndef PACKETGENERATOR_H
#define PACKETGENERATOR_H

#include <QRunnable>
#include <QThreadPool>
#include <QElapsedTimer>

#include "ireceiver.h"

class PacketGenerator : public QObject, public QRunnable {
    Q_OBJECT
private:
    IReceiver *rcv;
    QByteArray data;

    bool enabled;

    int size;
    int offset;
    int sendSize;
    quint64 fullSize;

    int randPacketSize();
    int randSize();
    int generatePacket();
    int generateTextPacket();
    int generateBinaryPacket();

public:
    explicit PacketGenerator(IReceiver *rcv, QObject *p=0);

    void run();
    void disable();

signals:
    void flowUpdated(float);
    void packetsCountUpdated(quint64);
};

#endif // PACKETGENERATOR_H
