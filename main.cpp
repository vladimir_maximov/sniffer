#include "snifferwidget.h"
#include <QApplication>
#include "sniffer.h"

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);

    SnifferWidget w;
    w.show();

    return a.exec();
}
