#ifndef SNIFFERWIDGET_H
#define SNIFFERWIDGET_H

#include <QWidget>
#include <QDebug>
#include <QThreadPool>

#include "sniffercallback.h"
#include "sniffer.h"
#include "packetgenerator.h"

QT_BEGIN_NAMESPACE
class QPushButton;
class QLineEdit;
QT_END_NAMESPACE

class SnifferWidget : public QWidget
{
    Q_OBJECT
    Sniffer *sniffer;
    SnifferCallback cb;

    QThreadPool pool;
    PacketGenerator *gen;

    QPushButton *startGenBtn;
    QPushButton *stopGenBtn;

    QLineEdit *binaryPacketsEdit;
    QLineEdit *textPacketsEdit;
    QLineEdit *totalPacketsEdit;
    QLineEdit *flowEdit;

public:
    SnifferWidget(QWidget *parent = 0);
    ~SnifferWidget();

public slots:
    void onGenStartClicked();
    void onGenStopClicked();

    void onFlowUpdated(float);
    void onPacketsCountUpdated(quint64);
    void onTxtCounterUpdated(quint64);
    void onBinCounterUpdated(quint64);
};

#endif // SNIFFERWIDGET_H
