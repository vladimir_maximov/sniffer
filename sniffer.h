#ifndef SNIFFER_H
#define SNIFFER_H

#include <QByteArray>

#include "ireceiver.h"
#include "icallback.h"

class Sniffer : public IReceiver
{
    // перечисление состояний при разборе потока
    //
    enum Action
    {
        aHeader,
        aCR1,
        aLF1,
        aCR2,
        aLF2,
        aBinarySize,
        aBinaryData
    };

private:
    const char binaryHeader = 0x24;
    const char cr = '\r';
    const char lf = '\n';

    const quint32 bufferSize = 0x10000000;  // размер кольцевого буфера
    const quint32 mask = bufferSize - 1;    // маска кольцевого буфера
    QByteArray buffer;  // кольцевой буфер
    quint32 readPos;    // указатель чтения
    quint32 writePos;   // указатель записи

    const quint32 defaultResultSize = 0x100000;
    QByteArray result;    // буфер результатов

    ICallback *callback;

    Action action;
    int dataSize;

    quint8 binarySizeIndex;
    QByteArray binarySize;

private:
    // обработка данных в кольцевом буфере
    void process();

    inline void callbackBinaryPacket(const char *data, unsigned int size)
    { if (callback) callback->BinaryPacket(data, size); }

    inline void callbackTextPacket(const char *data, unsigned int size)
    { if (callback) callback->TextPacket(data, size); }

    // очистка буфера результатов
    inline void clearResult()
    { result.resize(0); }

    // увеличение буфера результатов
    inline void extendResult(int size)
    { result.reserve(qMax(result.capacity(), size)); }

    // добавление значение в буфер результатов
    inline void addValue(char c)
    {
        if (result.size() == result.capacity())
            result.reserve(result.capacity() + defaultResultSize);
        result.append(c);
    }

public:
    explicit Sniffer(ICallback *c);
    ~Sniffer();

    void Receive(const char *data, unsigned int size);
};

#endif // SNIFFER_H
